var express = require ('express');
var bodyParser     =        require("body-parser");
var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var gameid = 0;
var pole = [];
var poles = [];

app.get('/',function (request,response){
	//console.log(request);
	//console.log(response);
	response.sendFile(__dirname + '/new 1.html');
});
app.get('/game',function (request,response){
	//console.log(request.query);
	response.render(__dirname + '/game.ejs', {'name': request.query.name});
});
app.get('/game1',function (request,response){
	//console.log(request.query);
	gameid += 1;
	for (var ct=1;ct<4;ct++){
		pole[ct] = new Array();
		for (var rd=1;rd<4;rd++){
			pole[ct][rd] = 99
	}};
	poles[gameid] = pole;
	response.render(__dirname + '/game1.ejs', {'name': request.query.name});
});
app.get('/game2',function (request,response){
	//console.log(request.query);
	gameid += 1;
	for (var ct=1;ct<4;ct++){
		pole[ct] = new Array();
		for (var rd=1;rd<4;rd++){
			pole[ct][rd] = 99
	}};
	poles[gameid] = pole;
	response.render(__dirname + '/game2.ejs', {'name': request.query.name});
});
app.post('/move', function (request,response){
	//console.log(request.body);
	pole[request.body.x][request.body.y]=1;
	/*for (var s=1;s<4;s++){
	for (var n=1;n<4;n++){
	if (pole[s][n] == 99) {
		pole[s][n] = 2;
		response.send({'x':s, 'y':n});
		exit;
	}}}*/
	var p = false;
	for (var s=1;s<4;s++){
	for (var n=1;n<4;n++){
		if (n==1) {
			if (pole[s][n] == pole[s][n+1] && pole[s][n] != 99){
				if(pole[s][n+2]==99){
					pole[s][n+2]=2;
					p=true;
					response.send({'x':s, 'y':n+2});
					return;
				}
			}
			if (pole[s][n] == pole[s][n+2] && pole[s][n] != 99){
				if(pole[s][n+1]==99){
					pole[s][n+1]=2;
					p=true;
					response.send({'x':s, 'y':n+1});
					return;
				}
			}
		}
		if (n==2){
			if (pole[s][n]==pole[s][n+1] && pole[s][n] != 99){
				if(pole[s][n-1]==99){
					pole[s][n-1]=2;
					p=true;
					response.send({'x':s, 'y':n-1});
					return;
				}
			}
			
		}
		if (s==1){	
			if (pole[s][n]==pole[s+1][n] && pole[s][n] != 99){
				if(pole[s+2][n]==99){
					p=true;
					pole[s+2][n]=2;
					response.send({'x':s+2, 'y':n});
					return;
				}
			}
			if (pole[s][n]==pole[s+2][n] && pole[s][n] != 99){
				if(pole[s+1][n]==99){
					p=true;
					pole[s+1][n]=2;
					response.send({'x':s+1, 'y':n});
					return;
				}
			}
		}
		if (s==2){
			if (pole[s][n]==pole[s+1][n] && pole[s][n] != 99){
				if(pole[s-1][n]==99){
					p=true;
					pole[s-1][n]=2;
					response.send({'x':s-1, 'y':n});
					return;
				}
			}
			
		}
	}};
	//console.log(p);
	if (p==false){
		for (var s=1;s<4;s++){
			for (var n=1;n<4;n++){
				//console.log("in for " + pole[s][n]);
				if (pole[s][n] == 99) {
					
					pole[s][n] = 2;
					response.send({'x':s, 'y':n});
					return;
				}
			}
		}
	}
		
});

app.use(express.static('public'));

//app.listen('1234','127.0.0.1');
/*console.log(util.inspect(process.env)) 
var server_port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    server_ip_address   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
*/const PORT = process.env.PORT || 5000 


var http = require('http');
var server = http.createServer(app);
server.listen(PORT);


var users = [];
var sockets = [];
var length = 0;
var game = {gamer1: undefined, gamer2: undefined};

var io = require('socket.io').listen(server);
io.sockets.on('connection', function (client) {
	console.log(client);
	sockets[client.conn.id] = length;
	users[length] = {client: client};
	length++;
	game.gamer1 = client;
	
	
	client.on('username', function (message) {
		console.log(message);
		console.log("message name for :" + client.conn.id +"       " + message.name);
		users[sockets[client.conn.id]].name = message.name;
		console.log(users);
		console.log("\n\n");
		
		var gamers = [];
		for (var i=0; i<users.length; i++){
			if(users[i]!=undefined && users[i].name != undefined)
				gamers[gamers.length] = users[i].name;
		}
		client.emit('gamerslist', {gamers: gamers});
		client.broadcast.emit('gamerslist', {gamers: gamers});
	});
	
	client.on("selectgamer", function (data) {
		
	});
	
    //подписываемся на событие message от клиента
	client.on('message', function (message) {
        try {
            //посылаем сообщение себе
            client.emit('message', message);
            //посылаем сообщение всем клиентам, кроме себя
            client.broadcast.emit('message', message);
        } catch (e) {
            //console.log(e);
            client.disconnect();
        }
    });
	client.on('player', function(message) {
		console.log(message);
		game.gamer1 = client;
		for (var i=0; i<users.length; i++){
			if (users[i]!=undefined && users[i].name == message.gamer) {
				game.gamer2 = users[i].client;
				//console.log(users[i]);
			}
		}
		game.gamer1.emit("gamestarted", {canmove: true});
		game.gamer2.emit("gamestarted", {canmove: false});
	});
	client.on('move', function(message) {
		if (client == game.gamer1) {
			pole[message.x][message.y] = 1;
			game.gamer2.emit("nextmove",{x: message.x, y: message.y});
		}	else {
			pole[message.x][message.y] = 2;
			game.gamer1.emit("nextmove",{x: message.x, y: message.y});
		}		
	});
	
	client.on('disconnect', function () {
		console.log(client);
		console.log("\n\n");
		console.log(sockets);
		console.log("\n\n");
		console.log(users[sockets[client.conn.id]].name + " disconnected");
		delete users[sockets[client.conn.id]];
		if client==game.gamer1)
			game.gamer2.emit("disconnected");
		else if (client == game.gamer2)
			game.gamer1.emit("disconnected");
		io.emit('user disconnected');
  });
	
	client.on('chatmsg',function(message){
		client.broadcast.emit("chatmsg",{gamer:message.gamer, txt:message.txt});
		client.emit ("chatmsg",{gamer:message.gamer, txt:message.txt});
	});
	
	
//client.on
	
	
	
	
	
	
  
  
});